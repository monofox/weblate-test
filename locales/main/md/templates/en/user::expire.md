% Title: Account will expire shortly

Dear {{user.name}},

your account will be deleted shortly for either not having logged in for more than {{expirePeriod}} days or for never having logged in at all. 

Please log in soon.

Your Lerntools-Team