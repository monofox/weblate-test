% Title: Password Reset

Dear {{user.name}},

A password reset was initiated for your account on page [{{instanceUrl}}]({{instanceUrl}}).

Please click the following link to complete the process:
[{{confirmUrl}}]({{confirmUrl}})

If you did not start this request yourself, please ignore this email.