% Title: Transaction Code

Dear {{user.name}},

The code to confirm your action is: {{tan}}.

If you have not requested a code, please ignore this message.