% Title: Account deleted

Dear {{user.name}},

your account and all associated data have been deleted. 
This process cannot be undone - but you can request a new account at any time.

Your Lerntools-Team