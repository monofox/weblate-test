% Title: Register Account

Hi {{user.login}},

You have registered an account on page {{instanceUrl}}.

Please click the following link to complete the process:
[{{confirmUrl}}]({{confirmUrl}})

If you did not start this request yourself, please ignore this email.

Your Lerntools-Team