% Title: Register Account

A new user has registered on {{instanceUrl}}

Name: {{user.name}}
Email: {{user.email}}
Comment: {{user.comment}}