% Title: Konto gelöscht

Ein Konto wurde wg Inaktivität von [{{config.SITE_NAME}}]({{instanceUrl}}) gelöscht:

Name: {{user.name}}
Email: {{user.email}}
Letzter Login: {{user.lastLogin}}