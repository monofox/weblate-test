% Title: Kontoregistrierung

Ein neuer Benutzer hat sich auf {{instanceUrl}} registriert:

Name: {{user.name}}
Email: {{user.email}}
Kommentar: {{user.comment}}
