% Title: Kontoregistrierung

Hallo {{user.login}},

Sie haben ein Konto auf der Seite {{instanceUrl}} registriert.

Bitte klicken Sie auf den folgenden Link, um den Vorgang abzuschließen:
[{{confirmUrl}}]({{confirmUrl}})

Wenn Sie diese Anfrage nicht selbst gestartet haben, ignorieren Sie bitte diese E-Mail.

Ihr Lerntools-Team