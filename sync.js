/*
 * Configuration for Lerntools (don't change if you really know what you'r doing!)
 */

// Supported locales - this entry **MUST** contain at least the entries from export.LOCALES in ./base/consts.js
// see https://de.wikipedia.org/wiki/Liste_der_ISO-639-1-Codes
const LOCALES = ['de', 'en']
// Default language (containig all keys)
const DEFAULT_LANG = "de"
// Location of the weblate repo locales file, relative to the position of this script
const WEBLATE_REPO = "./locales/"
// Location of lerntools base repo, relativ to the position of this script
const BASE_REPO = "./base/"
// Directories containing vue-locale files of the client repo
const LOCALE_DIRS = {
  "main": "main/locales/",
  "abcd": "modules/abcd/locales/",
  "ideas": "modules/ideas/locales/",
  "projector": "modules/projector/locales/",
  "survey": "modules/survey/locales/"
}
// Push changes automatically?
const PUSH_AUTO = false

/*
 * Don't change anything from here
 */

// Imports
const fs = require('fs')
const path = require('path')
const readlineSync = require('readline-sync');
const { execSync } = require('child_process');

// check for repositories and pull
function pullRepositories() {
  // pull weblate repo
  if (!fs.existsSync(WEBLATE_REPO) || !fs.lstatSync(WEBLATE_REPO).isDirectory() ) {
    console.error('Error weblate directory does not exist.')
    process.exit(1)
  }
  execSync('cd ' + WEBLATE_REPO + '; git pull')
  // pull client repo
  if (!fs.existsSync(BASE_REPO) || !fs.lstatSync(BASE_REPO).isDirectory() ) {
    console.error('Error client directory does not exist.')
    process.exit(1)
  }
  execSync('cd ' + BASE_REPO + '; git pull')
}

// push changes to repositories
function pushRepositories() {
  // weblate repo
  execSync('cd ' + WEBLATE_REPO + '; git add *.json; git commit -m "Update locales"; git push;')
  // client repo
  for (const [moduleId, moduleDir] of Object.entries(LOCALE_DIRS)) {
    execSync('cd ' + BASE_REPO + '; git add ' + moduleDir)
  }
  execSync('cd ' + BASE_REPO + '; git commit -m "Update locales"; git push;')
}

// main sync function
function doSync() {
  // loop through modules of application (main, mint, etc.)
  for (const [moduleId, moduleDir] of Object.entries(LOCALE_DIRS)) {
    let weblates = {}

    // 1. Read all weblate files and create if missing
    for (const lang of LOCALES) {
      let dir = path.join(WEBLATE_REPO, moduleId)
      try {
          // first check if directory already exists
          if (!fs.existsSync(dir)) {
              fs.mkdirSync(dir);
              console.log("Directory " + dir + " is created.");
          }
      } catch (err) {
          console.log(err);
      }
      let tmpName = path.join(dir, lang + '.json')
      let tmp = {}
      try {
        // create new weblate file, if missing
        if (!fs.existsSync(tmpName)) {
          logWeblate('Create file', moduleId, lang)
          fs.writeFileSync(tmpName, JSON.stringify(tmp));
        }
        // read data from weblate file
        tmp = JSON.parse(fs.readFileSync(tmpName));
      }
      catch (e) {
        console.error('Error: Unable to read weblate file: ' + tmpName + '. Please check if the file and contains valid json.')
        console.error(e.message)
        process.exit(1)
      }
      weblates[lang]=tmp
    }

    // 2. Read all locale files
    let locales = {}
    let localeFileList = []
    try {
      localeFileList = fs.readdirSync(path.join(BASE_REPO, moduleDir))
    } catch (e) {
      console.error('Error during processing ' + moduleDir + ': ' + e.message)
    }
    // Exit if the list is empty - this is very likely an error
    if (!localeFileList || localeFileList.length === 0) {
      console.error('Error: Can\'t read json file from folder ' + moduleDir)
      process.exit(1)
    }
    // Read all json files and exit on error
    for (let file of localeFileList) {
      if (file.endsWith('json')) {
        try {
          let componentName = file.substring(0, file.indexOf('.json'))
          locales[componentName] = JSON.parse(fs.readFileSync(path.join(BASE_REPO, moduleDir, file)))
        }
        catch (e) {
          console.error('Error: Unable to read locale file: ' + file + '. Please check if the file and contains valid json.')
          console.error(e.message)
          process.exit(1)
        }
      }
    }

    // 3. Check consistency of locale files
    for (const lang of LOCALES) {
      for (const component of Object.keys(locales) ) {
        // Add language to locale if missing
        if (!locales[component][lang]) {
          locales[component][lang]={}
          logLocale('Add lang', moduleId, lang, component)
        }
        for (const key of Object.keys(locales[component][lang])) {
          // Remove keys withing locales if missing in DEFAULT_LANG
          if (!locales[component][DEFAULT_LANG].hasOwnProperty(key)) {
            delete locales[component][lang][key]
            logLocale("Delete key ", moduleId, lang, component, key)
          }
        }
      }
    }

    // 4. Add missing components and keys to weblate
    for ( const lang of LOCALES) {
      for ( const component of Object.keys(locales) ) {
        // add missing components to weblate
        if (!weblates[lang][component]) {
          weblates[lang][component] = {}
          logWeblate('Add component', moduleId, lang, component)
        }
        // add missing keys to weblate
        if (locales[component][lang]) {
          for ( const key of Object.keys(locales[component][lang])) {
            if (locales[component][lang][key] && !weblates[lang][component][key]) {
              weblates[lang][component][key] = locales[component][lang][key]
              logWeblate('Add key', moduleId, lang, component, key)
            }
          }
        }
      }
    }

    // 5. Remove unused components from weblate
    for (const lang of LOCALES) {
      for (const component of Object.keys(weblates[lang])) {
        if (!locales.hasOwnProperty(component)) {
          delete weblates[lang][component]
          logWeblate("Delete component ", moduleId, lang, component)
        }
      }
    }

    // 6. Copy changes from weblate to locale files
    for (const lang of LOCALES) {
      for (const component of Object.keys(locales) ) {
        for (const key of Object.keys(weblates[lang][component])) {
          // Remove from weblate when removed from DEFAULT_LANG
          if (!locales[component][DEFAULT_LANG].hasOwnProperty(key)) {
            delete weblates[lang][component][key]
            logWeblate("Delete key ", moduleId, lang, component, key)
          }
          // Update in locale if changed
          if (weblates[lang][component][key] !== '' && weblates[lang][component][key] !== locales[component][lang][key]) {
            let newValue = weblates[lang][component][key]
            // Ask user for action if locale key already exists
            if (locales[component][lang][key] !== '' && locales[component][lang][key] !== undefined) {
              logLocale("Conflict ", moduleId, lang, component, key)
              console.log('(1) ' + weblates[lang][component][key] + ' (DEFAULT, from weblate)')
              console.log('(2) ' + locales[component][lang][key] + ' (from client repo)')
              const answer = askQuestion('Please Choose string: ')
              if (answer === '2' ) newValue = locales[component][lang][key]
            }
            // Use weblate value if locales value is empty
            else {
              logLocale("Update key ", moduleId, lang, component, key)
            }
            // set new value
            locales[component][lang][key] = newValue
            weblates[lang][component][key] = newValue
          }
        }
      }
    }

    // 7. Save weblate files
    for (const lang of LOCALES) {
      let tmpName = path.join(WEBLATE_REPO, moduleId, lang + '.json')
      fs.writeFileSync(tmpName, JSON.stringify(weblates[lang], null, 4))
    }

    // 8. Save locale files
    for (const lang of LOCALES) {
      for (const component of Object.keys(locales)) {
        let tmpName = path.join(BASE_REPO, moduleDir, component + '.json')
        fs.writeFileSync(tmpName, JSON.stringify(locales[component], null, 2))
      }
    }
  }
}

// internal log function for changes to weblate files
function logWeblate(msg, moduleId, lang, component, key) {
  let log = 'Weblate: ' + msg + ' ' + moduleId + ' → ' + lang
  if (component) log += ' → ' + component
  if (key) log += ' → ' + key
  console.log(log)
}

// internal log function for changes to client/locale files
function logLocale(msg, moduleId, lang, component, key) {
  let log = 'Locales: ' + msg + ' ' + moduleId + ' → ' + component
  if (lang) log += ' → ' + lang
  if (key) log += ' → ' + key
  console.log(log)
}

// read user input
function askQuestion(query) {
  const answer = readlineSync.question(query);
  return answer
}

// run
function main() {
  if (askQuestion('1. Please push all changes from weblate, put weblate into maintainance mode and enter "Ye$": ') !== 'Ye$') process.exit(1)
  console.log("---\n2. Pulling client and weblate repository")
  pullRepositories()
  console.log("---\n3. Syncing between weblate and client locales")
  doSync()
  if (PUSH_AUTO) {
    console.log("---\n4. Pushing changes to weblate and client locales\n")
    pushRepositories()
  }
  else {
    askQuestion("---\n4. Please push all changes in weblate and client repos manually and press [ENTER]")
  }
  askQuestion('---\n5. Please perform pull in weblate and press [Enter]')
}

main()
